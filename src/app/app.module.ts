import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { CoreModule } from '@core/core.module';


const modules = [ BrowserModule, AppRoutingModule, BrowserAnimationsModule, CoreModule ];

@NgModule({
  declarations: [ AppComponent ],
  imports: [ modules ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
