import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarMonthViewBodyDayComponent } from './calendar-month-view-body-day.component';

describe('Calendar Month View Body Day Component', () => {
  let component: CalendarMonthViewBodyDayComponent;
  let fixture: ComponentFixture<CalendarMonthViewBodyDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarMonthViewBodyDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarMonthViewBodyDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });
});
