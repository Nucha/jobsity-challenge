import { Component, OnInit, Input } from '@angular/core';
import { Reminder } from '@app/interfaces/reminder';
import { CalendarDate } from '@app/interfaces/calendar-date';

@Component({
  selector: 'app-calendar-month-view-body-day',
  templateUrl: './calendar-month-view-body-day.component.html',
  styleUrls: ['./calendar-month-view-body-day.component.scss']
})
export class CalendarMonthViewBodyDayComponent implements OnInit {

  @Input() reminders: Reminder[];
  @Input() date: CalendarDate[];
  edit = 'Edit';
  constructor() { }

  ngOnInit() { }

}
