import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarMonthViewBodyComponent } from './calendar-month-view-body.component';

describe('CalendarMonthViewBodyComponent', () => {
  let component: CalendarMonthViewBodyComponent;
  let fixture: ComponentFixture<CalendarMonthViewBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarMonthViewBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarMonthViewBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
