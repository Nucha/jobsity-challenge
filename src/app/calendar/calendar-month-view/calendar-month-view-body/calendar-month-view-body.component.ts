import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
// import * as moment from 'moment';

import { ReminderService } from '@core/reminder.service';
import { Reminder } from '@interfaces/reminder';
import { CalendarDate } from '@interfaces/calendar-date';

@Component({
  selector: 'app-calendar-month-view-body',
  templateUrl: './calendar-month-view-body.component.html',
  styleUrls: ['./calendar-month-view-body.component.scss']
})
export class CalendarMonthViewBodyComponent implements OnInit {

  @Input() daysInMonth: CalendarDate[];
  numberOfDaysInWeek = 7;
  message = 'Add reminder';

  constructor(private reminderService: ReminderService, private router: Router) { }

  ngOnInit() {
    // this.getReminders();
  }

  getReminders(day: CalendarDate): Reminder[] {
    return this.reminderService.getRemindersOfTheDay(day);
  }

  addReminder(day: CalendarDate) {
    this.router.navigate(['/reminder', { day: day.date.date(), month: day.date.month(), year: day.date.year() }]);
  }
}
