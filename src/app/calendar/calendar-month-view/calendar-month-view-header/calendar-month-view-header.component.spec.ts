import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarMonthViewHeaderComponent } from './calendar-month-view-header.component';

describe('CalendarMonthViewHeaderComponent', () => {
  let component: CalendarMonthViewHeaderComponent;
  let fixture: ComponentFixture<CalendarMonthViewHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarMonthViewHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarMonthViewHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
