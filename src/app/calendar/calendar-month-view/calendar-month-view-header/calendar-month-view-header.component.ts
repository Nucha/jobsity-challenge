import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DaysOfWeek } from '@app/enums/days-of-week.enum';
import * as moment from 'moment';

@Component({
  selector: 'app-calendar-month-view-header',
  templateUrl: './calendar-month-view-header.component.html',
  styleUrls: ['./calendar-month-view-header.component.scss']
})
export class CalendarMonthViewHeaderComponent {

  @Input() date: moment.Moment;
  @Output() nextMonth = new EventEmitter();
  @Output() previousMonth = new EventEmitter();

  next = 'Next Month';
  previous = 'Previous Month';
  daysOfWeek = [ DaysOfWeek.SUNDAY, DaysOfWeek.MONDAY, DaysOfWeek.TUESDAY, DaysOfWeek.WEDNESDAY, DaysOfWeek.THURSDAY, DaysOfWeek.FRIDAY, DaysOfWeek.SATURDAY ];
  constructor() { }

  getPreviousMonth() {
    this.previousMonth.emit();
  }

  getNextMonth() {
    this.nextMonth.emit();
  }
}
