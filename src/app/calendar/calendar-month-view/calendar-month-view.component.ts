import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import { CalendarService } from '@core/calendar.service';
import { CalendarDate } from '@app/interfaces/calendar-date';

@Component({
  selector: 'app-calendar-month-view',
  templateUrl: './calendar-month-view.component.html',
  styleUrls: ['./calendar-month-view.component.scss']
})
export class CalendarMonthViewComponent implements OnInit {

  selectedDate: moment.Moment;
  daysInSelectedMonth: CalendarDate[];
  constructor(private calendarService: CalendarService) { }

  ngOnInit() {
    this.selectedDate = this.calendarService.getCurrentDate();
    this.getDaysInSelctedMonth();
   }

  onPreviousMonth() {
    this.selectedDate = this.calendarService.getPreviousMonth(this.selectedDate);
    this.getDaysInSelctedMonth();
  }

  onNextMonth() {
    this.selectedDate = this.calendarService.getNextMonth(this.selectedDate);
    this.getDaysInSelctedMonth();
  }

  getDaysInSelctedMonth() {
    this.daysInSelectedMonth = this.calendarService.getDaysOfMonth(this.selectedDate);
  }
}
