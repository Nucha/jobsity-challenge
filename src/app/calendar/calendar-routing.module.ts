import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalendarComponent } from '@calendar/calendar.component';
import { ReminderComponent } from '@calendar/reminder/reminder.component';

const calendarRoutes: Routes = [
  {
    path: '',
    component: CalendarComponent
  },
  {
    path: 'reminder',
    component: ReminderComponent
  }
];

@NgModule({
  imports: [ RouterModule.forChild(calendarRoutes) ],
  exports: [ RouterModule ]
})
export class CalendarRoutingModule { }
