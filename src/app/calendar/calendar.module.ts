import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@shared/shared.module';
import { CalendarRoutingModule } from '@calendar/calendar-routing.module';
import { CalendarComponent } from '@calendar/calendar.component';
import { CalendarMonthViewComponent } from '@calendar/calendar-month-view/calendar-month-view.component';
import { CalendarMonthViewHeaderComponent } from '@calendar/calendar-month-view/calendar-month-view-header/calendar-month-view-header.component';
import { CalendarMonthViewBodyComponent } from '@calendar/calendar-month-view/calendar-month-view-body/calendar-month-view-body.component';
import { CalendarMonthViewBodyDayComponent } from '@calendar/calendar-month-view/calendar-month-view-body/calendar-month-view-body-day/calendar-month-view-body-day.component';
import { ReminderComponent } from './reminder/reminder.component';

const componentsList = [ CalendarComponent, CalendarMonthViewComponent, CalendarMonthViewHeaderComponent, CalendarMonthViewBodyComponent,
                        CalendarMonthViewBodyDayComponent ];

@NgModule({
  declarations: [ componentsList, ReminderComponent ],
  imports: [ CommonModule, CalendarRoutingModule, SharedModule, FormsModule, ReactiveFormsModule ]
})
export class CalendarModule { }
