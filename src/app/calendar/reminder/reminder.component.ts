import { Component, OnInit } from '@angular/core';
import { Reminder } from '@interfaces/reminder';
import * as moment from 'moment';
import { FormControl, Validators } from '@angular/forms';

import { ReminderService } from '@core/reminder.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.scss']
})
export class ReminderComponent implements OnInit {

  title = 'Add new reminder';
  reminderFormControl = new FormControl('', [ Validators.required ]);
  timeFormControl = new FormControl('', [ Validators.required ]);
  colorFormControl = new FormControl('', [ Validators.required ]);
  reminder: Reminder = {text: '', date: moment(), color: ''};

  constructor(private reminderService: ReminderService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    const day = this.route.snapshot.paramMap.get('day');
    const month = this.route.snapshot.paramMap.get('month');
    const year = this.route.snapshot.paramMap.get('year');
    this.reminder.date.day(day).month(month).year(Number(year));
  }

  onTimeChange(event): void {
    this.reminder.date.hour(Number(event.target.value.substring(0, 2))).minute(Number(event.target.value.substring(5, 3)));
  }

  addReminder() {
    const newReminder: Reminder = {text: this.reminder.text, date: this.reminder.date, color: this.reminder.color};
    console.log(this.reminder);
    this.reminderService.addNewReminder(newReminder);
    this.router.navigate(['/']);
  }

  isButtonDisnabled() {
    return (this.reminderFormControl.hasError('required') || this.timeFormControl.hasError('required') || this.colorFormControl.hasError('required'));
  }

}
