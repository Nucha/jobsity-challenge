import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';

import { CalendarDate } from '@interfaces/calendar-date';

@Injectable({ providedIn: 'root' })
export class CalendarService {

  constructor() { }

  getCurrentDate(): moment.Moment {
    return moment();
  }

  getNextMonth(date: moment.Moment): moment.Moment {
    return moment(date).add(1, 'months');
  }

  getPreviousMonth(date: moment.Moment): moment.Moment {
    return moment(date).subtract(1, 'months');
  }

  getDaysOfMonth(currentMoment: moment.Moment): CalendarDate[] {
    const firstOfMonth = moment(currentMoment).startOf('month').day();
    const lastDayOfMonth = moment(currentMoment).endOf('month').day();
    const firstDayOfGrid = moment(currentMoment).startOf('month').subtract(firstOfMonth, 'days');
    const start = firstDayOfGrid.date();
    const sixWeeks = 42;
    const fiveWeeks = 35;
    const weekToPlus = (lastDayOfMonth === 0) ? sixWeeks : fiveWeeks;
    return _.range(start, start + weekToPlus)
            .map((date: number): CalendarDate => {
              const d = moment(firstDayOfGrid).date(date);
              return { date: d };
            });
  }

}
