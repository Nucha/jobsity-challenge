import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavbarComponent } from '@core/navbar/navbar.component';
import { PageNotFoundComponent } from '@core/page-not-found/page-not-found.component';
import { SharedModule } from '@shared/shared.module';

const componentsList = [ NavbarComponent, PageNotFoundComponent ];

@NgModule({
  declarations: [ componentsList ],
  imports: [ CommonModule, SharedModule ],
  exports: [ componentsList ]
})
export class CoreModule { }
