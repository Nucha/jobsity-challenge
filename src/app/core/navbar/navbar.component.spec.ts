import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from '@core/navbar/navbar.component';
import { SharedModule } from '@shared/shared.module';

describe('Navbar Component', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ SharedModule ],
      declarations: [ NavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'Calendar'`, () => {
    expect(component.title).toEqual('Calendar');
  });
});
