import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNotFoundComponent } from '@core/page-not-found/page-not-found.component';
import { SharedModule } from '@shared/shared.module';

describe('Page Not Found Component', () => {
  let component: PageNotFoundComponent;
  let fixture: ComponentFixture<PageNotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ SharedModule ],
      declarations: [ PageNotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have a message error`, () => {
    expect(component.message).toEqual('Upss!!! Page not found...');
  });
});
