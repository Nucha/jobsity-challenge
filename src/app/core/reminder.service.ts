import { Injectable } from '@angular/core';
import * as moment from 'moment';

import { Reminder } from '@interfaces/reminder';
import { CalendarDate } from '@interfaces/calendar-date';

@Injectable({ providedIn: 'root' })
export class ReminderService {

  remindersList: Reminder[] = [];
  // {text: 'prueba 1', date: moment()},
  // {text: 'prueba 2', date: moment().add(15, 'days')}, {text: 'prueba 3', date: moment().add(1, 'days')}
  constructor() { }

  addNewReminder(newReminder: Reminder): void {
    this.remindersList.push(newReminder);
  }

  getRemindersOfTheDay(day: CalendarDate): Reminder[] {
    const filteredList = this.remindersList.filter((reminder: Reminder) => reminder.date.isSame(day.date, 'day'));
    return filteredList.sort(this.AscendingTimeCompareFn);
  }

  AscendingTimeCompareFn = (a: CalendarDate, b: CalendarDate) => (a.date.isAfter(b.date, 'minute')) ? 1 : ((a.date.isBefore(b.date, 'minute')) ? -1 : 0);

  deleteReminder(): void {
    return;
  }

  deleteAllRemindersFromDate(): void {

  }

}
