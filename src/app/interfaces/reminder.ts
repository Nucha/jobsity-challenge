import * as moment from 'moment';

export interface Reminder {
    text: string;
    date: moment.Moment;
    color: string;
}
