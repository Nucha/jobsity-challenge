import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';


const materialModules = [ MatToolbarModule, MatCardModule, MatGridListModule, MatIconModule, MatTooltipModule,
                          MatInputModule, MatButtonModule ];

@NgModule({
  imports: [ materialModules ],
  exports: [ materialModules ]
})
export class SharedModule { }
